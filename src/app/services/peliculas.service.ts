import { Injectable } from '@angular/core';
import { Pelicula } from '../models/pelicula';

@Injectable()

export class PeliculaService{
    public peliculas:Pelicula[];
    constructor(){
        this.peliculas = [
            new Pelicula("spiderman 4",2019,"https://as.com/meristation/imagenes/2018/12/21/noticias/1545373739_112630_1545373800_noticia_normal.jpg"),
            new Pelicula("Los vengadores endgame",2019,"https://i.blogs.es/d1f406/avengers-endgame-poster-cropped/1366_2000.jpg"),
            new Pelicula("Batman vs Superman",2015,"https://www.ecestaticos.com/imagestatic/clipping/c81/cc8/c81cc8dd808a2deb28239b758bd9cdd2.jpg"),
          ];
    }
    holaMundo(){
        return 'Hola Mundo desde un servicio de angular';
    }
    getPeliculas(){
        return this.peliculas;
    }
}