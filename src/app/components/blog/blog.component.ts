import { Component, OnInit } from '@angular/core';
import { Global } from '../../services/global';
import { Article } from '../../models/article';
import { ArticleService } from '../../services/article.service';
@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.css'],
  providers: [ArticleService]
})
export class BlogComponent implements OnInit {
  public articles: Article[];
  public url:string;

  constructor(
    private _articleService: ArticleService
  ) {
    this.url =Global.url;
   }

  ngOnInit(): void {
    this._articleService.getArticles().subscribe(
      response =>{
        console.log(response);
        if(response.articles){
          this.articles = response.articles;
        }else{

        }
      },
      error => {
        console.log(error)
      }
    );
  }

}
