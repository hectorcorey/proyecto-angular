import { Component, OnInit , DoCheck , OnDestroy } from '@angular/core';
import { Pelicula } from '../../models/pelicula';
import { PeliculaService } from '../../services/peliculas.service';
@Component({
  selector: 'peliculas',
  templateUrl: './peliculas.component.html',
  styleUrls: ['./peliculas.component.css'],
  providers: [PeliculaService]
})
export class PeliculasComponent implements OnInit , DoCheck, OnDestroy {

  public titulo:string;
  public peliculas:Array<Pelicula>;
  public favorita:Pelicula;
  public fecha:any;

  constructor(private _peliculaService:PeliculaService) { 
    console.log('Constructor lanzado');
    this.titulo = "componente de peliculas";
    this.peliculas = this._peliculaService.getPeliculas();
    this.fecha = new Date(2020,8,12);
  }
  
  ngOnInit(): void {
    console.log("Componente cargado!!");
    console.log(this._peliculaService.holaMundo())
  }
  
  ngDoCheck(): void {
    console.log("DoCheck lanzado")
  }
  
  cambiarTitulo(){
    this.titulo = "El titulo ha sido cambiado";
  }
  
  ngOnDestroy(): void {
    console.log("El componente de va a eliminar")
  }
  mostrarFavorita(event){
    console.log(event);
    this.favorita =  event.pelicula;
  }

}
